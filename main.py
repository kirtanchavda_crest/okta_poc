import pkce
import requests
import uuid
import json
import secrets
import re


# Static parameters, not to be changed from client-side.
STATE = uuid.uuid4()
NONCE = secrets.token_urlsafe(32)

SCOPE = "openid profile email"
REDIRECT_URL = "<enter>"
CLIENT_ID = "<enter>"
OKTA_URL = "<enter>"

# Dynamic parameters, to be taken from client-side.
USERNAME = "<enter>"
PASSWORD = "<enter>"

print_line = "------------------------------------------------------------------"


# ------------------------------------------------------------------------ #
# Step 1: Generate code verifier and code challenge
# ------------------------------------------------------------------------ #
CODE_VERIFIER, CODE_CHALLENGE = pkce.generate_pkce_pair()


# ------------------------------------------------------------------------ #
# Step 2: Get the session token & session cookie using username & password
# ------------------------------------------------------------------------ #
payload = json.dumps(
    {
        "username": USERNAME,
        "password": PASSWORD,
        "options": {
            "multiOptionalFactorEnroll": True,
            "warnBeforePasswordExpired": True,
        },
    }
)

headers_session = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

response = requests.post(
    url=f"https://{OKTA_URL}/api/v1/authn",
    headers=headers_session,
    data=payload,
)

token = response.json()

SESSION_TOKEN = token["sessionToken"]

print(f"{print_line}\nSession Token: {SESSION_TOKEN}\n{print_line}")


# ------------------------------------------------------------------------ #
# Step 3: Get the Authorization Code using the session token
# ------------------------------------------------------------------------ #
url = f"https://{OKTA_URL}/oauth2/v1/authorize?client_id={CLIENT_ID}&response_type=code&response_mode=form_post&scope={SCOPE}&redirect_uri={REDIRECT_URL}&state={STATE}&nonce={NONCE}&code_challenge_method=S256&code_challenge={CODE_CHALLENGE}&sessionToken={SESSION_TOKEN}"

response = requests.get(url)

state_resp = re.findall('<input\s+type="hidden"\s+name="state"\s+value="([A-Za-z0-9_\-]*)"\s*/>', response.text)
code_resp = re.findall('<input\s+type="hidden"\s+name="code"\s+value="([A-Za-z0-9_\-]*)"\s*/>', response.text)

state_value = state_resp[0]
code_value = code_resp[0]


# Verify the STATE parameter
if state_value == str(STATE):
    print(f"State parameter verified successfully.\n{print_line}")
else:
    print(f"! State parameter is not verified !\n{print_line}")

# Authorization Code
AUTH_CODE = code_value

print(f"Authorization Code: {AUTH_CODE}\n{print_line}")


# ------------------------------------------------------------------------ #
# Step 4: Get the access_token using the Authorization Code
# ------------------------------------------------------------------------ #
response = requests.post(
    url=f"https://{OKTA_URL}/oauth2/v1/token",
    data={
        "grant_type": "authorization_code",
        "client_id": CLIENT_ID,
        "redirect_uri": REDIRECT_URL,
        "code": AUTH_CODE,
        "code_verifier": CODE_VERIFIER,
    },
)

# Access Token
ACCESS_TOKEN = response.json()["access_token"]

print(f"Access Token: {ACCESS_TOKEN}\n{print_line}")


# ------------------------------------------------------------------------ #
# Step 5: Verify the Access Token
# ------------------------------------------------------------------------ #

url = f"https://{OKTA_URL}/oauth2/v1/introspect"

payload = f"client_id={CLIENT_ID}&token={ACCESS_TOKEN}&token_type_hint=access_token"

headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
}

response = requests.post(url, headers=headers, data=payload)

result = response.json()

# Verify access_token
if response.status_code == 200 and result["active"] is True:
    print(f"Token verified successfully.\n{print_line}")
else:
    print(f"! Error in verifying the access_token !\n{print_line}")


# ------------------------------------------------------------------------ #
# Step 6: Get userinfo using the Access Token
# ------------------------------------------------------------------------ #
response = requests.get(
    url=f"https://{OKTA_URL}/oauth2/v1/userinfo",
    headers={
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Bearer {ACCESS_TOKEN}",
    },
)

# Print Userinfo
if response.status_code == 200:
    result = json.dumps(response.json(), indent=3)
    print(f"User Data: \n{result}\n{print_line}")
else:
    print(f"! Error in getting the userinfo !\n{print_line}")
