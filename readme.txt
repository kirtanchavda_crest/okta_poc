Steps to run the demo:
- Install the required library modules provided in the requirements.txt file.
- Change the credentials variables in the main.py file.
- Run the main.py file.